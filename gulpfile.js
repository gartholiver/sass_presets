const gulp = require('gulp');
const sass = require('gulp-sass');
 
gulp.task('sass', () => {
  return gulp.src('./sass/style.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./build'));
});

gulp.task('default', () => {
  gulp.watch('./sass/**/*.scss', ['sass']);
});