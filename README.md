# Garth's Handy Dandy SASS Presets
A handy default setup for developing the stylesheet for a project. I like things
that are modular, so this is somewhat modular.

## Overview:
* config
  * _general.scss - General styles
  * _variables.scss - Universal variables

* utils
  * _form.scss - Forms, fields, buttons, etc.
  * _grid.scss - Grid system loosly based on Boostrap
  * _helpers.scss - Useful classes that can make styling quick and easy
  * _mixins.scss - Well... mixins

* _custom.scss - Custom styling, will start out empty
* style.scss - Where everything comes together via @import

### Note
Set up node modules seperately to prevent enviornmental binary issues (like if
you are using Linux one day and OSX the next =P).